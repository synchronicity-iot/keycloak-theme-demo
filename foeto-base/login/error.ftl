<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <#if properties.meta?has_content>
      <#list properties.meta?split(' ') as meta>
          <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
      </#list>
  </#if>
  <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
  <#if properties.styles?has_content>
      <#list properties.styles?split(' ') as style>
          <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
      </#list>
  </#if>
  <title>${msg("title")}</title>
</head>

<body>
  <div>
    <p>Der er sket en fejl</p>
  </div>
</body>

</html>