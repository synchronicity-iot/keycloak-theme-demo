<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <#if properties.meta?has_content>
      <#list properties.meta?split(' ') as meta>
          <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
      </#list>
  </#if>
  <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
  <#if properties.styles?has_content>
      <#list properties.styles?split(' ') as style>
          <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
      </#list>
  </#if>
  <title>${msg("title")}</title>
</head>

<body class="${properties.bodyClass!}">
  <div class="${properties.containerClass!}">
    <h2 class="${properties.headerTextClass!}">${msg("titleText")}</h2>
    <form class="${properties.formClass!}" action="${url.loginAction}" method="post">
      <div class="${properties.formGroupClass!}">
        <div class="${properties.labelWrapperClass!}">
          <label for="username" class="${properties.labelClass!}">${msg("username")}</label>
        </div>
        <div class="${properties.inputWrapperClass!}">
          <input id="username" class="${properties.inputClass!}" name="username" type="text" autofocus />
        </div>
      </div>
      <div class="${properties.formGroupClass!}">
        <div class="${properties.labelWrapperClass!}">
          <label for="password" class="${properties.labelClass!}">${msg("password")}</label>
        </div>
        <div class="${properties.inputWrapperClass!}">
          <input id="password" class="${properties.inputClass!}" name="password" type="password" />
        </div>
      </div>
      <div class="${properties.formGroupClass!}">
        <div class="${properties.buttonWrapperClass!}">
          <input type="submit" class="${properties.buttonClass}" value="${msg("logIn")}"></input>
        </div>
      </div>
    </form>
    <p class="${properties.bodyTextClass!}">Denne spørgeskemaundersøgelse er udarbejdet og finansieret i et samarbejde mellem:</p>
    <div class="${properties.imgGroupClass!}">
      <div class="${properties.imgContainerClass!}">
        <img class="${properties.imgClass!}" src="${url.resourcesPath}${properties.ai}" alt="Alexandra Instituttet">
      </div>
      <div class="${properties.imgContainerClass!}">
        <img class="${properties.imgClass!}" src="${url.resourcesPath}${properties.auh}" alt="Aarhus Universitetshospital">
      </div>
      <div class="${properties.imgContainerClass!}">
        <img class="${properties.imgClass!}" src="${url.resourcesPath}${properties.defactum}" alt="Defactum">
      </div>
      <div class="${properties.imgContainerClass!}">
        <img class="${properties.imgClass!}" src="${url.resourcesPath}${properties.rkkp}" alt="Regionernes Kliniske Kvalitetsudviklingsprogram">
      </div>
    </div>
  </div>
</body>

</html>